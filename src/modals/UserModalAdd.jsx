import { useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";

function UserModalAdd({ users, setUsers, createData }) {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const id = users.length + 1;
  const [username, setUsername] = useState(null);
  const [group_name, setGroupname] = useState(null);

  const newUser = { id, username, group_name };
  // console.log(newUser);

  const handleSubmit = (e) => {
    e.preventDefault();
    createData(newUser, "http://localhost:8000/users/", users, setUsers);
    handleClose();
  };

  return (
    <>
      <Button variant="primary" onClick={handleShow}>
        Agregar ususario
      </Button>

      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton className="bg-secondary p-3">
          <Modal.Title>Agregar usuario</Modal.Title>
        </Modal.Header>
        <Modal.Body className="bg-dark p-3">
          <Form style={{ display: "block", padding: "3rem" }}>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>ID</Form.Label>
              <Form.Control
                type="text"
                placeholder="ID del producto"
                disabled
                value={id}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Nombre</Form.Label>
              <Form.Control
                type="text"
                placeholder="Nombre del usuario"
                autoFocus
                onChange={(e) => setUsername(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Grupo</Form.Label>
              <Form.Control
                type="text"
                placeholder="Grupo del usuario"
                onChange={(e) => setGroupname(e.target.value)}
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer className="bg-dark p-3">
          <Button variant="secondary" onClick={handleClose}>
            Cerrar
          </Button>
          <Button variant="primary" type="submit" onClick={handleSubmit}>
            Guardar
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default UserModalAdd;

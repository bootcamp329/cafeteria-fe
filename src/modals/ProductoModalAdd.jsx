import { useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";

function ProductoModalAdd({ products, setProducts, createData }) {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const id = products.length + 1;
  const [nombre, setNombre] = useState(null);
  const [precio, setPrecio] = useState(null);

  const newProduct = { id, nombre, precio };
  // console.log(newProduct);

  const handleSubmit = (e) => {
    e.preventDefault();
    createData(
      newProduct,
      "http://localhost:8000/productos/",
      products,
      setProducts
    );
    handleClose();
  };

  return (
    <>
      <Button variant="primary" onClick={handleShow}>
        Agregar producto
      </Button>

      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton className="bg-secondary p-3">
          <Modal.Title>Agregar producto</Modal.Title>
        </Modal.Header>
        <Modal.Body className="bg-dark p-3">
          <Form style={{ display: "block", padding: "3rem" }}>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>ID</Form.Label>
              <Form.Control
                type="text"
                placeholder="ID del producto"
                disabled
                value={id}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Nombre</Form.Label>
              <Form.Control
                type="text"
                placeholder="Nombre del producto"
                autoFocus
                onChange={(e) => setNombre(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Precio</Form.Label>
              <Form.Control
                type="text"
                placeholder="Precio del producto"
                onChange={(e) => setPrecio(e.target.value)}
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer className="bg-dark p-3">
          <Button variant="secondary" onClick={handleClose}>
            Cerrar
          </Button>
          <Button variant="primary" type="submit" onClick={handleSubmit}>
            Guardar
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default ProductoModalAdd;

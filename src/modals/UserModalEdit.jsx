import { useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";

function UserModalEdit({
  show,
  handleClose,
  user,
  users,
  setUsers,
  updateData,
}) {
  const id = user.id;

  const [username, setUsername] = useState(user.username);
  const [group_name, setGroupname] = useState(user.group_name);

  const updatedUser = { id, username, group_name };

  const handleSubmit = (e) => {
    e.preventDefault();
    updateData(updatedUser, "http://localhost:8000/users", users, setUsers);
    handleClose();
  };

  return (
    <>
      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton className="bg-secondary p-3">
          <Modal.Title>Editar usuario</Modal.Title>
        </Modal.Header>
        <Modal.Body className="bg-dark p-3">
          <Form style={{ display: "block", padding: "3rem" }}>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>ID</Form.Label>
              <Form.Control
                type="text"
                placeholder="ID del usuario"
                disabled
                value={id}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Nombre</Form.Label>
              <Form.Control
                type="text"
                placeholder="Nombre del usuario"
                autoFocus
                value={username}
                onChange={(e) => setUsername(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Grupo</Form.Label>
              <Form.Control
                type="text"
                placeholder="Precio del producto"
                value={group_name}
                onChange={(e) => setGroupname(e.target.value)}
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer className="bg-dark p-3">
          <Button variant="secondary" onClick={handleClose}>
            Cerrar
          </Button>
          <Button variant="primary" type="submit" onClick={handleSubmit}>
            Guardar
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default UserModalEdit;

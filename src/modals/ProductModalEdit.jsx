import { useState } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";

function ProductModalEdit({
  show,
  handleClose,
  product,
  products,
  setProducts,
  updateData,
}) {
  const id = product.id;

  const [nombre, setNombre] = useState(product.nombre);
  const [precio, setPrecio] = useState(product.precio);

  const updatedProduct = { id, nombre, precio };

  const handleSubmit = (e) => {
    e.preventDefault();
    updateData(
      updatedProduct,
      "http://localhost:8000/productos",
      products,
      setProducts
    );
    window.location.reload();
    handleClose();
  };

  return (
    <>
      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton className="bg-secondary p-3">
          <Modal.Title>Editar producto</Modal.Title>
        </Modal.Header>
        <Modal.Body className="bg-dark p-3">
          <Form style={{ display: "block", padding: "3rem" }}>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>ID</Form.Label>
              <Form.Control
                type="text"
                placeholder="ID del producto"
                disabled
                value={id}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Nombre</Form.Label>
              <Form.Control
                type="text"
                placeholder="Nombre del producto"
                autoFocus
                value={nombre}
                onChange={(e) => setNombre(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Precio</Form.Label>
              <Form.Control
                type="text"
                placeholder="Precio del producto"
                value={precio}
                onChange={(e) => setPrecio(e.target.value)}
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer className="bg-dark p-3">
          <Button variant="secondary" onClick={handleClose}>
            Cerrar
          </Button>
          <Button variant="primary" type="submit" onClick={handleSubmit}>
            Guardar
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default ProductModalEdit;

import { useEffect } from "react";
import CartCard from "./CartCard";

function CartListing({
  pedidos,
  setPedidos,
  products,
  role,
  updateData,
  // listo,
  // setListo,
  // servido,
  // setServido,
}) {
  return (
    <>
      <h2 className="mb-3 display-6">Pedidos</h2>
      <div className="d-flex flex-wrap justify-content-center gap-3">
        {pedidos.length === 0 ? (
          <h3 className="mb-5">Sin pedidos...</h3>
        ) : (
          <>
            {pedidos.map((pedido) => {
              return (
                <CartCard
                  key={pedido.id}
                  pedido={pedido}
                  products={products}
                  role={role}
                  updateData={updateData}
                  pedidos={pedidos}
                  setPedidos={setPedidos}
                  // listo={listo}
                  // setListo={setListo}
                  // servido={servido}
                  // setServido={setServido}
                />
              );
            })}
          </>
        )}
      </div>
    </>
  );
}

export default CartListing;

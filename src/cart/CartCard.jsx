import { useState } from "react";

function CartCard({
  pedido,
  products,
  role,
  updateData,
  pedidos,
  setPedidos,
  // listo,
  // setListo,
  // servido,
  // setServido,
}) {
  const id = pedido.id;
  const mesa = pedido.mesa;
  const lista_productos = pedido.lista_productos;
  const cantidad_productos = pedido.cantidad_productos;
  const monto_total = pedido.monto_total;

  const [listo, setListo] = useState(false);
  const [servido, setServido] = useState(pedido.servido);

  const [enviadoListo, setEnviadoListo] = useState(false);
  const [enviadoServido, setEnviadoServido] = useState(false);

  // console.log(pedido.listo);
  console.log(listo);

  const updatedCart = {
    id,
    mesa,
    lista_productos,
    cantidad_productos,
    monto_total,
    listo,
    servido,
  };

  // console.log(updatedCart);

  const ListoHandle = () => {
    setListo(!listo);
  };

  // console.log(listo);

  const ServidoHandle = () => {
    setServido(!servido);
  };

  const enviarListoHandle = (e) => {
    e.preventDefault();
    console.log(listo);
    console.log(updatedCart);
    updateData(
      updatedCart,
      "http://localhost:8000/pedidos",
      pedidos,
      setPedidos
    );
    setEnviadoListo(true);
  };

  const enviarServidoHandle = (e) => {
    e.preventDefault();
    console.log(servido);
    console.log(updatedCart);
    updateData(
      updatedCart,
      "http://localhost:8000/pedidos",
      pedidos,
      setPedidos
    );
    setEnviadoServido(true);
  };

  // console.log(servido);

  let disabled = enviadoServido;

  const chkBtn =
    role === "recepcionista" ? (
      <div className="form-check">
        <input
          className="form-check-input"
          type="checkbox"
          defaultValue=""
          id="flexCheckDefault"
          onChange={ListoHandle}
        />
        <label className="form-check-label" htmlFor="flexCheckDefault">
          Servir pedido
        </label>
      </div>
    ) : (
      <div className="form-check">
        <input
          className="form-check-input"
          type="checkbox"
          defaultValue=""
          id="flexCheckDefault"
          onChange={ServidoHandle}
          disabled={disabled}
        />
        <label className="form-check-label" htmlFor="flexCheckDefault">
          Entregar pedido
        </label>
      </div>
    );

  const btnAccion =
    role === "recepcionista" ? (
      <button
        type="submit"
        className="btn btn-primary"
        onClick={enviarListoHandle}
      >
        Entregar
      </button>
    ) : (
      <a className="btn btn-primary" onClick={enviarServidoHandle}>
        Servir
      </a>
    );

  const pedidoListo = enviadoListo ? (
    <div className="fs-3 bg-success rounded">Pedido Listo</div>
  ) : null;

  const pedidoServido =
    servido && role === "recepcionista" ? (
      <div className="fs-3 bg-info rounded">Pedido Servido</div>
    ) : null;

  return (
    <div className="card text-dark" style={{ width: "18rem" }}>
      <div className="card-body">
        {pedidoListo}
        {pedidoServido}
        <h5 className="card-title">Pedido n°{id}</h5>
        <div className="container text-center">
          <div className="row">
            <div className="col-md-4 px-1">
              <h6>Cant.</h6>
              <ul className="list-group mb-3">
                {pedido.cantidad_productos.map((y, index) => {
                  return (
                    <li
                      className="list-group-item lh-sm"
                      style={{ height: "3.5rem" }}
                      key={index}
                    >
                      {y}
                    </li>
                  );
                })}
              </ul>
            </div>
            <div className="col-md-8 px-1">
              <h6>Producto</h6>
              <ul className="list-group mb-3">
                {pedido.lista_productos.map((y) => {
                  const productExists = products.find((x) => x.id === y);
                  return (
                    <li
                      className="list-group-item lh-sm"
                      style={{ height: "3.5rem" }}
                      key={productExists.id}
                    >
                      {productExists.nombre}
                    </li>
                  );
                })}
              </ul>
            </div>
          </div>
        </div>
        {chkBtn}
        {btnAccion}
      </div>
    </div>
  );
}

export default CartCard;

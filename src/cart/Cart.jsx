import CartItem from "./CartItem";
import { toCurrencyFormat } from "../utils/format-utils";
import { useEffect, useState } from "react";

const getCartTotal = (cart) => {
  return toCurrencyFormat(
    cart.reduce((sum, cur) => sum + cur.qty * cur.precio, 0)
  );
};

const mesasHabilitadas = [1, 2, 3, 4, 5];

// let id;
// const uid = (() => (id = 1, () => id++))();

function Cart({
  cart,
  addToCart,
  removeFromCart,
  pedidos,
  setPedidos,
  createData,
}) {
  // Averiguar última id de mesa ocupada
  const idOcupadas = pedidos.map((x) => x.id);
  let ultimoId = idOcupadas[idOcupadas.length - 1] + 1;
  // id de la orden actual
  const [idCart, setIdCart] = useState(ultimoId);
  // Mesa de la orden actual
  const [mesaOrden, setMesaOrden] = useState();
  // Id de los productos en la orden
  const idCartProduct = cart.map((product) => product.id);
  // Cantidad de los productos en la orden
  const cantidadProductos = cart.map((product) => product.qty);
  // Monto total de la orden
  const monto_total = cart.reduce((a, c) => a + c.qty * c.precio, 0);

  // Mensaje de orden vacía
  let emptyCartMessage;
  if (cart.length === 0) {
    emptyCartMessage = <span>No hay items en la orden</span>;
  }

  // Verificar mesas ocupadas y disponibles
  const mesasOcupadas = pedidos.map((x) => x.mesa);
  const mesasDisponibles = mesasHabilitadas.filter(
    (e) => !mesasOcupadas.includes(e)
  );

  let fullTableMessage;
  if (mesasDisponibles.length === 0) {
    fullTableMessage = (
      <span style={{ color: "indianred" }}>No hay mesas disponibles!</span>
    );
  }

  const newCart = {
    id: idCart,
    mesa: mesaOrden,
    lista_productos: idCartProduct,
    cantidad_productos: cantidadProductos,
    monto_total: monto_total,
    listo: false,
    servido: false,
  };
  // // console.log(newProduct);

  const cartHandle = () => {
    // e.preventDefault();
    createData(newCart, "http://localhost:8000/pedidos/", pedidos, setPedidos);
    actualizarListaPedidos();
    resetForm();
    window.location.reload();
  };

  const mesaHandle = (e) => {
    setMesaOrden(e.target.value);
    console.log(mesaOrden);
  };

  const resetForm = () => {
    cart.length = 0;
    setMesaOrden("");
    localStorage.removeItem("cart");
    setIdCart(ultimoId + 1);
  };

  const actualizarListaPedidos = () => {
    const ordenCompleta = {
      id: idCart,
      mesa: mesaOrden,
      lista_productos: idCartProduct,
      listo: false,
      servido: false,
    };
    const listaActualizadaPedidos = [...pedidos, ordenCompleta];
    setPedidos(listaActualizadaPedidos);
  };

  return (
    <div>
      <h4 className="d-flex justify-content-between align-items-center mb-3">
        <span>Orden</span>
        <span className="badge bg-primary rounded-pill">{idCart}</span>
      </h4>
      {fullTableMessage}
      <select
        className="form-select mb-2"
        aria-label="Default select example"
        value={mesaOrden}
        onChange={mesaHandle}
      >
        <option defaultValue={"0"}>Seleccionar mesa</option>
        {mesasDisponibles.map((mesa) => (
          <option value={mesa} key={mesa}>
            Mesa n° {mesa}
          </option>
        ))}
      </select>
      {emptyCartMessage}
      <ul className="list-group mb-3">
        {cart.map((product, i) => (
          <CartItem
            cartItem={product}
            key={"cart" + i}
            addToCart={addToCart}
            removeFromCart={removeFromCart}
          />
        ))}
        <li className="list-group-item d-flex justify-content-between">
          <span>Total:</span>
          <strong>{getCartTotal(cart)}</strong>
        </li>
        <li className="list-group-item d-flex justify-content-center">
          <button className="btn btn-primary" onClick={cartHandle}>
            Enviar a cocina
          </button>
        </li>
      </ul>
    </div>
  );
}

export default Cart;

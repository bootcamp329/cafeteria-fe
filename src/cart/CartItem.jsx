import { toCurrencyFormat } from "../utils/format-utils";

function CartItem(props) {
  return (
    <li className="list-group-item d-flex justify-content-around align-items-center lh-sm">
      <div>
        <h6 className="my-0">{props.cartItem.nombre}</h6>
      </div>
      <span className="text-body-secondary">
        {props.cartItem.qty} x {toCurrencyFormat(props.cartItem.precio)}
      </span>
      <button
        className="btn btn-danger mx-1"
        onClick={() => props.removeFromCart(props.cartItem)}
      >
        -
      </button>
      <button
        className="btn btn-info mx-1"
        onClick={() => props.addToCart(props.cartItem)}
      >
        +
      </button>
    </li>
  );
}

export default CartItem;

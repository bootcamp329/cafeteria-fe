import Table from "react-bootstrap/Table";
import UserTableRow from "./UserTableRow";

function UserListing({ users, setUsers, updateData, deleteData }) {
  // console.log(users);
  return (
    <>
      <h2>Listado de Usuarios</h2>
      <Table striped bordered hover size="sm" variant="dark">
        <thead>
          <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Grupo</th>
            <th colSpan="2">Acciones</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user) => {
            return (
              <tr key={user.id}>
                <UserTableRow
                  user={user}
                  users={users}
                  setUsers={setUsers}
                  updateData={updateData}
                  deleteData={deleteData}
                />
              </tr>
            );
          })}
        </tbody>
      </Table>
    </>
  );
}

export default UserListing;

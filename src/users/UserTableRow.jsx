import React, { useState } from "react";
import UserModalEdit from "../modals/UserModalEdit";

function UserTableRow({ user, users, setUsers, updateData, deleteData }) {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
      <td>{user.id}</td>
      <td>{user.username}</td>
      <td>{user.group_name}</td>
      <td>
        <button type="button" className="btn btn-primary" onClick={handleShow}>
          Editar
        </button>
      </td>
      <td>
        <button
          type="button"
          className="btn btn-danger"
          onClick={() =>
            deleteData(user.id, "http://localhost:8000/users", users, setUsers)
          }
        >
          Eliminar
        </button>
      </td>

      <UserModalEdit
        show={show}
        handleClose={handleClose}
        user={user}
        users={users}
        setUsers={setUsers}
        updateData={updateData}
      ></UserModalEdit>
    </>
  );
}

export default UserTableRow;

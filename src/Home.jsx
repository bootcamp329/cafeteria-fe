import { useState, useEffect } from "react";
import coffeeIcon from "./assets/coffee.svg";
import { helpHttp } from "./helpers/helpHttp";
import ProductListing from "./products/ProductListing";
import Cart from "./cart/Cart";
import CartListing from "./cart/CartListing";
import Loader from "./utils/Loader";
import Message from "./utils/Message";
import ProductoModalAdd from "./modals/ProductoModalAdd";
import UserListing from "./users/UserListing";
import UserModalAdd from "./modals/UserModalAdd";

const Home = ({ onLogout, userId, cart, setCart }) => {
  const [user, setUser] = useState(null);
  const [users, setUsers] = useState(null);
  const [pedidos, setPedidos] = useState(null);
  const [products, setProducts] = useState(null);
  const [dataToEdit, setDataToEdit] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  // const [listo, setListo] = useState(false);
  // const [servido, setServido] = useState(false);

  let api = helpHttp();
  // Token de acceso
  let options = {
    headers: {
      Authorization: `Bearer ${JSON.parse(
        window.localStorage.getItem("accessToken")
      )}`,
    },
  };
  // Declarar endpoints
  let urlUser = "http://localhost:8000/users/" + userId;
  let urlUsers = "http://localhost:8000/users/";
  let urlProductos = "http://localhost:8000/productos/";
  let urlPedidos = "http://localhost:8000/pedidos/";

  // Recuperar usuario
  useEffect(() => {
    setLoading(true);
    helpHttp()
      .get(urlUser, options)
      .then((res) => {
        // console.log(res);
        if (!res.err) {
          setUser(res);
          setError(null);
        } else {
          setUser(null);
          setError(res);
        }
        setLoading(false);
      });
  }, [urlUser]);

  // Recuperar usuarios
  useEffect(() => {
    setLoading(true);
    helpHttp()
      .get(urlUsers, options)
      .then((res) => {
        // console.log(res);
        if (!res.err) {
          setUsers(res);
          setError(null);
        } else {
          setUsers(null);
          setError(res);
        }
        setLoading(false);
      });
  }, [urlUsers]);

  // Recuperar productos
  useEffect(() => {
    setLoading(true);
    helpHttp()
      .get(urlProductos, options)
      .then((res) => {
        // console.log(res);
        if (!res.err) {
          setProducts(res);
          setError(null);
        } else {
          setProducts(null);
          setError(res);
        }
        setLoading(false);
      });
  }, [urlProductos]);

  // Recuperar pedidos
  useEffect(() => {
    setLoading(true);
    helpHttp()
      .get(urlPedidos, options)
      .then((res) => {
        // console.log(res);
        if (!res.err) {
          setPedidos(res);
          setError(null);
        } else {
          setPedidos(null);
          setError(res);
        }
        setLoading(false);
      });
  }, [urlPedidos]);

  // Crear dato
  const createData = (data, url, db, setDb) => {
    // data.id = data.length + 1;
    //console.log(data);

    let options = {
      body: data,
      headers: {
        Authorization: `Bearer ${JSON.parse(
          window.localStorage.getItem("accessToken")
        )}`,
        "Content-Type": "application/json",
      },
    };

    api.post(url, options).then((res) => {
      //console.log(res);
      if (!res.err) {
        setDb([...db, res]);
      } else {
        setError(res);
      }
    });
  };

  // Actualizar datos
  const updateData = (data, url, db, setDb) => {
    let endpoint = `${url}/${data.id}/`;
    //console.log(endpoint);

    let options = {
      body: data,
      headers: {
        Authorization: `Bearer ${JSON.parse(
          window.localStorage.getItem("accessToken")
        )}`,
        "Content-Type": "application/json",
      },
    };

    api.put(endpoint, options).then((res) => {
      //console.log(res);
      if (!res.err) {
        let newData = db.map((el) => (el.id === data.id ? data : el));
        setDb(newData);
      } else {
        setError(res);
      }
    });
  };

  // Eliminar dato
  const deleteData = (id, url, db, setDb) => {
    let isDelete = window.confirm(
      `¿Estás seguro de eliminar el registro con el id '${id}'?`
    );

    if (isDelete) {
      let endpoint = `${url}/${id}/`;
      let options = {
        headers: {
          Authorization: `Bearer ${JSON.parse(
            window.localStorage.getItem("accessToken")
          )}`,
          "Content-Type": "application/json",
        },
      };

      api.del(endpoint, options).then((res) => {
        //console.log(res);
        if (!res.err) {
          let newData = db.filter((el) => el.id !== id);
          setDb(newData);
        } else {
          setError(res);
        }
      });
    } else {
      return;
    }
  };

  // Manejador de logout
  const logoutHandler = () => {
    onLogout();
  };

  // Agregar a orden
  function addToCart(product) {
    const exist = cart.find((x) => x.id === product.id);
    if (exist) {
      const newCart = cart.map((x) =>
        x.id === product.id ? { ...exist, qty: exist.qty + 1 } : x
      );
      setCart(newCart);
      localStorage.setItem("cart", JSON.stringify([...cart, product]));
    } else {
      const newCart = [...cart, { ...product, qty: 1 }];
      setCart(newCart);
      localStorage.setItem("cart", JSON.stringify([...cart, product]));
    }
  }

  // Eliminar de orden
  function removeFromCart(product) {
    const exist = cart.find((x) => x.id === product.id);
    if (exist.qty === 1) {
      const newCart = cart.filter((x) => x.id !== product.id);
      setCart(newCart);
      localStorage.setItem("cart", JSON.stringify(newCart));
    } else {
      const newCart = cart.map((x) =>
        x.id === product.id ? { ...exist, qty: exist.qty - 1 } : x
      );
      setCart(newCart);
      localStorage.setItem("cart", JSON.stringify(newCart));
    }
  }

  // Verificar rol
  const role = user ? user.group_name : null;

  // Mostrar boton de agregar producto
  const addProduct =
    role && role === "admin" ? (
      <div className="col-md-5 col-lg-4 mx-auto">
        <ProductoModalAdd
          products={products}
          setProducts={setProducts}
          createData={createData}
        />
      </div>
    ) : null;

  // Mostrar lista de productos a Recepcionista o Admin
  const content =
    role && role !== "cocinero" ? (
      <ProductListing
        cart={cart}
        setCart={setCart}
        addToCart={addToCart}
        products={products}
        setProducts={setProducts}
        role={role}
        updateData={updateData}
        deleteData={deleteData}
      />
    ) : null;

  // Mostrar boton de agregar cliente
  const addUser =
    role && role === "admin" ? (
      <div className="col-md-5 col-lg-4 mx-auto">
        <UserModalAdd
          users={users}
          setUsers={setUsers}
          createData={createData}
        />
      </div>
    ) : null;

  // Mostrar lista de usuarios a Admin
  const usersList = role && role === "admin" && (
    <div className={"col-md-12 col-lg-12 mt-1"}>
      <UserListing
        users={users}
        setUsers={setUsers}
        updateData={updateData}
        deleteData={deleteData}
      />
    </div>
  );

  // Mostrar orden
  const cartContent =
    role && role === "recepcionista" ? (
      <Cart
        cart={cart}
        addToCart={addToCart}
        removeFromCart={removeFromCart}
        pedidos={pedidos}
        setPedidos={setPedidos}
        createData={createData}
      />
    ) : null;

  // Mostrar lista de pedidos
  const listadoOrdenes =
    role && role !== "admin" ? (
      <CartListing
        pedidos={pedidos}
        setPedidos={setPedidos}
        products={products}
        role={role}
        updateData={updateData}
        // listo={listo}
        // setListo={setListo}
        // servido={servido}
        // setServido={setServido}
      />
    ) : null;

  return (
    <div className="container">
      <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark border-bottom">
        <div className="container-fluid">
          <a
            className="navbar-brand"
            href="#"
            style={{ display: "flex", alignItems: "center" }}
          >
            <img
              className="bi me-2"
              src={coffeeIcon}
              alt="Coffee Icon"
              width={50}
            />
            <span className="fs-4">Coffee Time</span>
          </a>
          <button onClick={logoutHandler}>Logout</button>
        </div>
      </nav>
      {user && (
        <main className="mt-4">
          <div className="pt-5 text-center">
            <h2 className="display-4">Bienvenido/a {user.username}!</h2>
          </div>
          <div className="row g-5 bg-dark mt-1">
            {addProduct}
            <div
              className={`col-md-${
                role === "recepcionista" ? "6" : "12"
              } col-lg-12 mt-1 px-1`}
            >
              {loading && <Loader />}
              {error && (
                <Message
                  msg={`Error ${error.status}: ${error.statusText}`}
                  bgColor="#dc3545"
                />
              )}
              {!loading && content}
            </div>
            {addUser}
            {loading && <Loader />}
            {error && (
              <Message
                msg={`Error ${error.status}: ${error.statusText}`}
                bgColor="#dc3545"
              />
            )}
            {!loading && usersList}
            <div className="col-md-6 col-lg-4 px-1">{cartContent}</div>
            <div className="mt-0 order-md-last">
              {loading && <Loader />}
              {error && (
                <Message
                  msg={`Error ${error.status}: ${error.statusText}`}
                  bgColor="#dc3545"
                />
              )}
              {!loading && listadoOrdenes}
            </div>
          </div>
        </main>
      )}
    </div>
  );
};

export default Home;

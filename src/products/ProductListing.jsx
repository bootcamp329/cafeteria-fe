import { useEffect, useState } from "react";
import Table from "react-bootstrap/Table";
import { toCurrencyFormat } from "../utils/format-utils";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Modal from "react-bootstrap/Modal";
import ProductTableRow from "./ProductTableRow";

function ProductListing({
  cart,
  setCart,
  addToCart,
  products,
  setProducts,
  role,
  updateData,
  deleteData,
}) {
  // console.log(products);

  return (
    <>
      <h2>Listado de Productos</h2>
      <Table striped bordered hover size="sm" variant="dark">
        <thead>
          <tr>
            <th style={{ width: "1rem" }}>Descripción</th>
            <th>Precio</th>
            <th colSpan={role === "recepcionista" ? 1 : 2}>Acciones</th>
          </tr>
        </thead>
        <tbody>
          {products.map((product) => {
            return (
              <tr key={product.id}>
                <ProductTableRow
                  product={product}
                  role={role}
                  updateData={updateData}
                  deleteData={deleteData}
                  products={products}
                  setProducts={setProducts}
                  addToCart={addToCart}
                />
              </tr>
            );
          })}
        </tbody>
      </Table>
    </>
  );
}

export default ProductListing;

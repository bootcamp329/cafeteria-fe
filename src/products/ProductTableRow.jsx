import React, { useState } from "react";
import { toCurrencyFormat } from "../utils/format-utils";
import ProductModalEdit from "../modals/ProductModalEdit";

function Product({
  product,
  role,
  updateData,
  deleteData,
  products,
  setProducts,
  addToCart,
}) {
  // console.log(product);
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  function definicionBoton(product) {
    let acciones = [];
    if (role === "recepcionista") {
      acciones = [
        <td key="0">
          <button
            type="button"
            className="btn btn-primary"
            onClick={() => addToCart(product)}
          >
            Agregar
          </button>
        </td>,
      ];
    } else {
      acciones = [
        <td key="0">
          <button
            type="button"
            className="btn btn-primary"
            onClick={handleShow}
          >
            Editar
          </button>
        </td>,
        <td key="1">
          <button
            type="button"
            className="btn btn-danger"
            onClick={() =>
              deleteData(
                product.id,
                "http://localhost:8000/productos",
                products,
                setProducts
              )
            }
          >
            Eliminar
          </button>
        </td>,
      ];
    }
    return acciones;
  }

  return (
    <>
      <td>{product.nombre}</td>
      <td>{toCurrencyFormat(product.precio)}</td>
      {definicionBoton(product)}

      <ProductModalEdit
        show={show}
        handleClose={handleClose}
        product={product}
        products={products}
        updateData={updateData}
        setProducts={setProducts}
      ></ProductModalEdit>
    </>
  );
}

export default Product;

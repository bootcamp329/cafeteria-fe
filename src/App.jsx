import { useState, useEffect } from 'react'
import './App.css'
import Login from './Login'
import Home from './Home'
import jwtDecode from 'jwt-decode'


function App() {
  const [userId, setUserId] = useState(null);
  const [cart, setCart] = useState([]);

  useEffect(() => {
    setCart(
      localStorage.getItem('cart')
        ? JSON.parse(localStorage.getItem('cart'))
        :[]);
  },[]);

  useEffect(() => {
    const token = window.localStorage.getItem('accessToken')
    if (token){
      setUserId(jwtDecode(JSON.parse(token)).user_id)
    }
  }, [])

  const onLoginHandler = (userId) => {
    console.log(userId)
    setUserId(userId)
  }

  const onLogoutHandler = () => {
    setUserId(null)
    window.localStorage.removeItem('accessToken')
  }

  return (
    <>
      {userId ? (
        <Home onLogout={onLogoutHandler} userId={userId} cart={cart} setCart={setCart} />
      ) : (
        <Login onLogin={onLoginHandler} />
      )}
    </>
  )
}

export default App

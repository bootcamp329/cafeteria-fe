const toCurrencyFormat = (value) => {
  return value.toLocaleString('es-PY', {minimumFractionDigits: 0, maximumFractionDigits: 0}) + ' Gs'
}

export { toCurrencyFormat }